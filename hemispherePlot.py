import optparse

parser = optparse.OptionParser()
parser.add_option("-A","--alphas",default=0.01,dest="alphas")
parser.add_option("-O","--order",default=1,dest="order")
parser.add_option("-f","--file",default="analytic_O{order}_aS{alphas}.yoda")
parser.add_option("-L","--lc",default=False,action="store_true",dest="lc")
parser.add_option("-c","--collinear",default=True,action="store_false",dest="coll")
parser.add_option("--dpsi-lam",type=float,default=0.55,dest='lam')
parser.add_option("--dpsi-kt12max",type=float,default=0.5,dest='kt12max')
parser.add_option("--dpsi-kt12min",type=float,default=0.3,dest='kt12min')
(opts,args) = parser.parse_args()

import config
config.quad_precision = 0
from mymath import *
from nll import *
from qcd import CA, CF
from histogram import Histo1D
from scipy.special import gamma, hyp2f1
import numpy as np
import analytic

oas = int(opts.order)
aS = mn(opts.alphas)

def Fadd(rp):
    return myexp(-np.euler_gamma*rp)/gamma(1+rp)

def FheavyMass(rp):
    return myexp(-np.euler_gamma*rp)/gamma(1+rp/2)**2
        
def Fone(rp):
    return 1

# see e.g. hep-ph/0112156 and hep-ph/9801324
def broadHypGeo(rp):
    return pow(2,rp/2) * rp * gamma(rp/2)/gamma(2+rp/2) * hyp2f1(1,2,2+rp/2,-1)

def FtotBroad(rp):
    return myexp(-np.euler_gamma*rp)/gamma(1+rp) * broadHypGeo(rp)**2
    
def FwideBroad(rp):
    return myexp(-np.euler_gamma*rp)/gamma(1+rp/2)**2 * broadHypGeo(rp)**2


resumParams = {"Thrust":(1,1,Fadd),
               "HeavyMassSq":(1,1,FheavyMass),
               "TotalBroad":(1,0,FtotBroad),
               "WideBroad":(1,0,FwideBroad),
               "FC1.5":(1,-0.5,Fadd),
               "FC1":(1,0,Fadd),
               "FC0.5":(1,0.5,Fadd),
               "FC0":(1,1,Fadd),
               "y23":(2,0,Fone)}

alphas = AlphaS(mn('91.1876'),aS,oas)
mr = -0.6
nbin = 120
pname = '/LL_EvtShp/{SHAPE}'
hs = []
for obs in resumParams:
    histo = Histo1D(120,mr,0.0,'/LL_EvtShp/{}\n'.format(obs))
    a,b,F = resumParams[obs]
    nll = NLL(alphas,mn(str(91.2**2)),mn(a),mn(b),CA/2 if opts.lc else CF,opts.coll)
    for i in range(nbin):
        xm = histo.bins[i].xmin
        xp = histo.bins[i].xmax
        v = myexp(xm/(aS/2)) if obs == "y23" else myexp(xm/(aS))
        if oas < 0:
            rp = analytic.Rp(nll.alpha(nll.Q2,5),mylog(1/v),a,b,nll.Ca)
            ym = analytic.genResult(nll.alpha(nll.Q2),mylog(1/v),a,b,
                                    lambda a,b,c,d:1,nll.Ca,nll.Bl)*F(rp)
        else:
            l = nll.alpha(nll.Q2)*nll.alpha.beta0(5)/(2*m.pi)*mylog(1/v)
            if xm == 0 or 2*l/a > 1 or 2*l/(a+b) > 1:
                ym = 1
            else:
                ym = nll.sigma(v)  if xm != 0 else mn(1)
                rp = 2*nll.Ca*nll.rp(nll.as0,nll.b0,a,b,mylog(1/v))
                ym *= F(rp)
        histo.Fill((xm+xp)/mn(2),ym)
    hs += [histo]

nbin = 64
histo = Histo1D(nbin,0.0,1.0,pname.format(SHAPE="DPsi"))
lam = opts.lam
kt12max = opts.kt12max
kt12min = opts.kt12min
for i in range(nbin):
    xm = histo.bins[i].xmin
    xp = histo.bins[i].xmax
    rp = 4*CF/m.pi * lam / (1 - 2*alphas.beta0(5)/(2*m.pi)*lam)
    ym = (myexp(rp*mylog(kt12max))-myexp(rp*mylog(kt12min)))/m.pi
    histo.Fill((xm+xp)/mn(2),ym)
hs += [histo]

file = open(opts.file.format(**vars(opts)),"w")
for h in hs:
    file.write(str(h))
    file.write("\n\n")
file.close()
