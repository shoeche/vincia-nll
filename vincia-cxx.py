# Python interface to Pythia to use for cross checks of Python showers.
#
# Requires Pytiha8 compilation with
# './configure --with-python=/path/to/python/include'
# and path to valid Pythia 8.3 installation.

PYTHIAPATH="/Users/christian/Workspace/vincia/main"

# General.
import sys, optparse

# Pythia8.
cfg = open(PYTHIAPATH+"/Makefile.inc")
lib = PYTHIAPATH+"/lib"
for line in cfg:
    if line.startswith("PREFIX_LIB="): lib = line[11:-1]; break
print(lib)
sys.path.insert(0, lib)
import pythia8

# Python shower.
from analysis import Analysis
from particle import Particle
from vector import Vec4

# Command-line options.
parser = optparse.OptionParser()
parser.add_option("-e","--events",default=1000,dest="events")
parser.add_option("-f","--file",default="vincia-cxx",dest="histo")
parser.add_option("-L","--lc",default=False,action="store_true",dest="lc")
parser.add_option("-a","--asmz",default='0.118',dest="asmz")
parser.add_option("-A","--alphas",default=0.118,dest="alphas")
parser.add_option("-O","--order",default=1,dest="order")
parser.add_option("-C","--cut",default=1,dest="cut")
parser.add_option("-Q","--ecms",default='91.1876',dest="ecms")
parser.add_option("-K","--cluster",default=5,dest="cas")
(opts,args) = parser.parse_args()

# Analysis.
jetrat = Analysis(float(opts.alphas),int(opts.cas))

# Pythia8.
pythia = pythia8.Pythia()
# Main settings.
pythia.readString("Beams:eCM                 = "+str(opts.ecms))
pythia.readString("Beams:idA                 =  11")
pythia.readString("Beams:idB                 = -11")
pythia.readString("Main:numberOfEvents       = "+str(opts.events))
pythia.readString("Next:numberCount          = "+str(int(opts.events)/10))
pythia.readString("Next:numberShowProcess    = 0")
pythia.readString("Next:numberShowEvent      = 0")
pythia.readString("Main:timesAllowErrors     = 1")
# Process settings.
pythia.readString("WeakSingleBoson:ffbar2gmZ = on")
pythia.readString("23:onMode                 = off")
pythia.readString("23:onIfAny                = 1 2 3 4 5")
pythia.readString("SigmaProcess:alphaSvalue  ="+str(opts.asmz))
pythia.readString("SigmaProcess:alphaSorder  ="+str(opts.order))
pythia.readString("SigmaProcess:alphaEMorder = -1")
pythia.readString("SigmaProcess:factorScale1 = 1")
pythia.readString("PDF:lepton                = off")
pythia.readString("PartonLevel:MPI           = off")
pythia.readString("HadronLevel:all           = off")
# VINCIA settings.
pythia.readString("PartonShowers:model       = 2")
pythia.readString("Vincia:CheckAntennae      = off")
pythia.readString("Vincia:sectorShower       = off")
pythia.readString("Vincia:ewMode             = 0")
pythia.readString("Vincia:interleaveResDec   = off")
pythia.readString("Vincia:nGluonToQuark      = 0")
pythia.readString("Vincia:nFlavZeroMass      = 5")
pythia.readString("Vincia:alphaSvalue        ="+str(opts.asmz))
pythia.readString("Vincia:alphaSorder        ="+str(opts.order))
pythia.readString("Vincia:useCMW             = off")
pythia.readString("Vincia:renormMultFacEmitF = 1.0")
pythia.readString("Vincia:cutoffScaleFF      ="+str(opts.cut))
pythia.readString("Vincia:ThresholdMC        = 0.0")
pythia.readString("Vincia:ThresholdMB        = 0.0")
pythia.readString("Vincia:modeSLC            = "+("0" if opts.lc else "1"))
pythia.init()

# Generate events.
for iEvent in range(0, int(opts.events)):
    pythia.next();
    event, weight = ([],1.0)
    event.append(
        Particle(pythia.event[3].id(),
                 Vec4(pythia.event[3].e(),pythia.event[3].px(),
                      pythia.event[3].py(),pythia.event[3].pz()),
                 [pythia.event[3].col(),pythia.event[3].acol()],0,[0,0])
    )
    event.append(
        Particle(pythia.event[4].id(),
                 Vec4(pythia.event[4].e(),pythia.event[4].px(),
                      pythia.event[4].py(),pythia.event[4].pz()),
                 [pythia.event[4].col(),pythia.event[4].acol()],1,[0,0])
    )
    for iPtcl in range(6,pythia.event.size()):
        ptcl = pythia.event[iPtcl]
        if not ptcl.isFinal(): continue
        event.append(Particle(ptcl.id(),
                              Vec4(ptcl.e(),ptcl.px(),ptcl.py(),ptcl.pz()),
                              [ptcl.col(),ptcl.acol()],iPtcl-4,[0,0]))
    jetrat.Analyze(event,weight)
    if iEvent % (int(opts.events)/10) == 0: jetrat.Finalize(opts.histo)

# Histograms.
jetrat.Finalize(opts.histo)

# Statistics.
pythia.stat()
