
class Kernel:

    def __init__(self,flavs,Ca):
        self.flavs = flavs
        self.Ca = Ca

class Soft (Kernel):

    def Value(self,y,xT):
        return self.Ca*2*(1-mysqrt(xT)*(myexp(y)+myexp(-y)))/xT
    def Estimate(self,y,xT):
        return self.Ca*2/xT
    def Integral(self,xT0):
        return self.Ca*2*mylog(1/xT0)
    def GenerateZ(self,xT0):
        lg = mylog(1/xT0)
        return -lg/2 + rng.random()*lg

class Aqq (Kernel):

    def Value(self,y,xT):
        return self.Ca*(2/xT + myexp(-2*y) + myexp(2*y) + 1
                        - 2*myexp(-y)/mysqrt(xT)
                        - 2*myexp(y)/mysqrt(xT))
    def Estimate(self,y,xT):
        return self.Ca*2/xT
    def Integral(self,xT0):
        return self.Ca*2*mylog(1/xT0)
    def GenerateZ(self,xT0):
        lg = mylog(1/xT0)
        return -lg/2 + rng.random()*lg

class Aqg (Kernel):

    def Value(self,y,xT):
        return self.Ca*(2/xT + myexp(-2*y) + myexp(2*y)
                        + (-2+xT)*myexp(-y)/mysqrt(xT)
                        + (-4+xT)*myexp(y)/(2*mysqrt(xT))
                        - myexp(-3*y)*mysqrt(xT))
    def Estimate(self,y,xT):
        return self.Ca*2/xT
    def Integral(self,xT0):
        return self.Ca*2*mylog(1/xT0)
    def GenerateZ(self,xT0):
        lg = mylog(1/xT0)
        return -lg/2 + rng.random()*lg

class Agq (Kernel):

    def Value(self,y,xT):
        return self.Ca*(2/xT + myexp(-2*y) + myexp(2*y)
                        + (-2+xT)*myexp(y)/mysqrt(xT)
                        + (-4+xT)*myexp(-y)/(2*mysqrt(xT))
                        - myexp(3*y)*mysqrt(xT))
    def Estimate(self,y,xT):
        return self.Ca*2/xT
    def Integral(self,xT0):
        return self.Ca*2*mylog(1/xT0)
    def GenerateZ(self,xT0):
        lg = mylog(1/xT0)
        return -lg/2 + rng.random()*lg

class Agg (Kernel):

    def Value(self,y,xT):
        return self.Ca*(2/xT + myexp(-2*y) + myexp(2*y) - 2
                        + 2*(-1+xT)*myexp(-y)/mysqrt(xT)
                        + 2*(-1+xT)*myexp(y)/mysqrt(xT)
                        - myexp(-3*y)*mysqrt(xT)
                        - myexp(3*y)*mysqrt(xT))
    def Estimate(self,y,xT):
        return self.Ca*2/xT
    def Integral(self,xT0):
        return self.Ca*2*mylog(1/xT0)
    def GenerateZ(self,xT0):
        lg = mylog(1/xT0)
        return -lg/2 + rng.random()*lg

class Agx (Kernel):

    def Value(self,y,xT):
        return self.Ca*myexp(y)/(2.*mysqrt(xT))*(xT*myexp(2*y)
                                + (-1 + myexp(-y)*mysqrt(xT)
                                + myexp(y)*mysqrt(xT))**2)
    def Estimate(self,y,xT):
        return self.Ca*2/xT
    def Integral(self,xT0):
        return self.Ca*2*mylog(1/xT0)
    def GenerateZ(self,xT0):
        lg = mylog(1/xT0)
        return -lg/2 + rng.random()*lg

class Shower:

    def __init__(self,alpha,t0,coll,beta,rt0,flat,nmax,lc):
        self.nmax = nmax
        self.t0 = t0
        self.rt0 = rt0
        self.beta = beta
        if len(flat)!=2: self.flat = False
        else:
            self.flat = True
            self.lmin = flat[0]
            self.lmax = flat[1]
        self.alpha = alpha
        self.alphamax = alpha(self.t0)
        self.amode = 0 if self.alpha.order == -1 else 1
        if self.amode != 0:
            self.alphamax = (2*m.pi)/self.alpha.beta0(5)
        self.kernels = {}
        for fl1 in [1,2,3,4,5,21]:
            self.kernels[fl1] = {}
            for fl2 in [-1,-2,-3,-4,-5,21]:
                if fl1==21 and fl2==21:
                    if coll == 0: self.kernels[fl1][fl2] = Soft([fl1,fl2,21],CA/2)
                    else: self.kernels[fl1][fl2] = Agg([fl1,fl2,21],CA/2)
                elif fl1==21:
                    if coll == 0: self.kernels[fl1][fl2] = Soft([fl1,fl2,21],CA/2 if lc else CF/2+CA/4)
                    else: self.kernels[fl1][fl2] = Agq([fl1,fl2,21],CA/2 if lc else CF/2+CA/4)
                elif fl2==21:
                    if coll == 0: self.kernels[fl1][fl2] = Soft([fl1,fl2,21],CA/2 if lc else CF/2+CA/4)
                    else: self.kernels[fl1][fl2] = Aqg([fl1,fl2,21],CA/2 if lc else CF/2+CA/4)
                else:
                    if coll == 0: self.kernels[fl1][fl2] = Soft([fl1,fl2,21],CA/2 if lc else CF)
                    else: self.kernels[fl1][fl2] = Aqq([fl1,fl2,21],CA/2 if lc else CF)
        self.Ca = CA/2 if lc else CF
        self.Bl = -3./4. if coll&1 else -1.

    def dsigma(self,v):
        if self.alpha.order >= 0:
            print('Order \\alpha not supported')
            exit(1)
        cx = mylog(v)
        J = 1/(1+self.beta)
        aS = self.alpha(self.Q2,5)
        return myexp(-J*aS*self.Ca/(m.pi)* \
                     (cx**2/mn(2)-2*self.Bl*cx+mn(3)/2-v))* \
            (-J*aS*self.Ca/(m.pi)*(cx-2*self.Bl-v))

    # Antenna kinematics with c = yjk/(yij+yjk).
    def MakeKinematics(self,t,y,phi,pI,pK):
        # Fetch branching variables and calculate invariants.
        Q2 = 2*pI.SmallMLDP(pK)
        Q, tau, xi = mysqrt(Q2), mysqrt(t/Q2), myexp(y)
        yij, yjk = tau/xi, tau*xi
        yik = 1-yij-yjk
        if yij < 0 or yjk < 0 or yik < 0: return []

        # Construct transverse vector.
        kt1 = pI.Cross(Rotation(pI,pK).y)
        if kt1.P2() == 0: return []
        kt1 *= mycos(phi)/kt1.P()
        uijk = pI*pK[0]-pK*pI[0]
        if uijk[3] == 0 and \
           pI[1] == -pK[1] and \
           pI[2] == -pK[2]: return []
        kt2 = kt1.Cross(uijk)
        kt2[0] = pI*kt1.Cross(pK)
        if kt2.M2() == 0: return []
        kt2 *= mysin(phi)/mysqrt(abs(kt2.M2()))
        pT = kt1 + kt2

        # Construct branching parameters.
        norm = 4*yij*yjk-yij-yjk
        rt   = mysqrt(1-4*yij*yjk/(yij+yjk))
        ai   =  -0.5*yij - (rt*yij)/2. - yjk/2. - (rt*yjk)/2. + (5*yij*yjk)/2. \
            + (3*rt*yij*yjk)/2. + yjk**2/2. + (rt*yjk**2)/2. - 2*yij*yjk**2
        ak   = -0.5*yij + (rt*yij)/2. + yij**2/2. - (rt*yij**2)/2. - yjk/2. \
            + (rt*yjk)/2. + (5*yij*yjk)/2. - (3*rt*yij*yjk)/2. - 2*yij**2*yjk
        bi   = -0.5*yij + (rt*yij)/2. - yjk/2. + (rt*yjk)/2. + (5*yij*yjk)/2. \
            - (3*rt*yij*yjk)/2. + yjk**2/2. - (rt*yjk**2)/2. - 2*yij*yjk**2
        bk   = -0.5*yij - (rt*yij)/2. + yij**2/2. + (rt*yij**2)/2. - yjk/2. \
            - (rt*yjk)/2. + (5*yij*yjk)/2. + (3*rt*yij*yjk)/2. - 2*yij**2*yjk
        c    = yjk/(yij+yjk)
        ai  /= norm
        ak  /= norm
        bi  /= norm
        bk  /= norm
        kt   = mysqrt(-Q2*yij*yjk*yik*(yij+yjk)/norm)
        pT  *= kt

        # Construct post-branching momenta.
        pi = ai*pI + bi*pK - c*pT
        pj = (1-ai-ak)*pI + (1-bi-bk)*pK + pT
        pk = ak*pI + bk*pK - (1-c)*pT
        return [pi,pj,pk]

    # Dipole-like kinematics with switch between recoiling dipole end.
    def MakeKinematicsPS(self,t,y,phi,pI,pK):
        # Fetch branching variables and calculate invariants.
        Q2 = 2*pI.SmallMLDP(pK)
        Q, tau, xi = mysqrt(Q2), mysqrt(t/Q2), myexp(y)
        yij, yjk = tau/xi, tau*xi
        yik = 1-yij-yjk
        if yij < 0 or yjk < 0 or yik < 0: return []

        # Construct transverse vector.
        kt1 = pI.Cross(Rotation(pI,pK).y)
        if kt1.P2() == 0: return []
        kt1 *= mycos(phi)/kt1.P()
        uijk = pI*pK[0]-pK*pI[0]
        if uijk[3] == 0 and \
           pI[1] == -pK[1] and \
           pI[2] == -pK[2]: return []
        kt2 = kt1.Cross(uijk)
        kt2[0] = pI*kt1.Cross(pK)
        if kt2.M2() == 0: return []
        kt2 *= mysin(phi)/mysqrt(abs(kt2.M2()))
        pT = kt1 + kt2

        # ij-collinear.
        if yij<yjk:
            ai  = yik/(yjk+yik)
            ak  = mn(0)
            bi  = yij*yjk/(yjk+yik)
            bk  = yjk+yik
            c   = mn(1)
            omc = mn(0)
            kt  = mysqrt(Q2*yij*yjk*yik/(yjk+yik))
            pT *= kt
        else:
            ai  = yij+yik
            ak  = yij*yjk/(yij+yik)
            bi  = mn(0)
            bk  = yik/(yij+yik)
            c   = mn(0)
            omc = mn(1)
            kt = mysqrt(Q2*yij*yjk*yik/(yij+yik))
            pT *= kt

        # Construct post-branching momenta.
        pi = ai*pI + bi*pK - c*pT
        pj = (1-ai-ak)*pI + (1-bi-bk)*pK + pT
        pk = ak*pI + bk*pK - omc*pT
        return [pi,pj,pk]

    # Original VINCIA antenna kinematics (Kosower map).
    def MakeKinematicsKosower(self,t,y,phi,pI,pK):
        Q2 = 2*pI.SmallMLDP(pK)
        Q, tau, xi = mysqrt(Q2), mysqrt(t/Q2), myexp(y)
        yij, yjk = tau/xi, tau*xi
        yik = 1-yij-yjk
        if yij < 0 or yjk < 0 or yik < 0: return []
        kt = mysqrt(yij*yik*yjk)
        ei, ej, ek = (yij+yik)/2, (yij+yjk)/2, (yik+yjk)/2
        cosij, cosik = 1-yij/(2*ei*ej), 1-yik/(2*ei*ek)
        sinij, sinik = kt/(2*ei*ej), kt/(2*ei*ek)
        # Set up kinematics in CM frame, with i oriented along positive z axis.
        cp, sp = mycos(phi), mysin(phi)
        pi = Q*ei*Vec4(mn(1),mn(0),mn(0),mn(1))
        pj = Q*ej*Vec4(mn(1),-sinij*cp,-sinij*sp,cosij)
        pk = Q*ek*Vec4(mn(1),sinik*cp,sinik*sp,cosik)
        # Kosower map.
        r, omr = yjk/(yij+yjk), yij/(yij+yjk)
        c   = (2*yij*yjk/(yij+yjk))**2
        rho = mysqrt(1+c/yik)
        y00 = -(-c/(1+rho) + 2*r*yij*yjk)/(8*ei*ek)
        # y00 = -2*yij*yjk**2*((yjk-yij)+rho*(yij+yjk)) \
        #     /((1+rho)*(yij+yjk)**2)/(8*ei*ek)
        cpsi, spsi = 1+2*y00, mysqrt(1-(1+2*y00)**2)
        if tau < mn(1e-3):
            cpsi = 1 - (2*tau**2*xi**3*(xi+tau*(1+xi**2)))/(1+xi**2)**2
        if tau < mn(1e-7):
            spsi = (tau*(16*xi**2*(1+xi**2) \
                         + (5*tau**3*(1+4*xi**2))/xi \
                         + 2*tau*xi*(4+8*xi**2+4*xi**4) \
                         + 2*tau**2*(3+9*xi**2+xi**4+3*xi**6))) \
                         /(8*(1+xi**2)**2)
        # ARIADNE map.
        # psi = ek**2/(ei**2+ek**2)*myacos(cosik)
        # cpsi, spsi = mycos(psi), mysin(psi)
        # Set up rotation.
        zvec = Vec4(mn(1),mn(0),mn(0),mn(1))
        krot = Rotation(zvec,Vec4(mn(1),spsi*cp,spsi*sp,cpsi))
        # Rotate and boost to lab frame.
        pold = pI+pK
        pIcm = pold.Boost(pI,Q2)
        pKcm = pold.Boost(pK,Q2)
        if pIcm.P2() == 0: return []
        rot = Rotation(zvec,pIcm)
        pi = pold.BoostBack(rot*(krot*pi),Q2)
        pj = pold.BoostBack(rot*(krot*pj),Q2)
        pk = pold.BoostBack(rot*(krot*pk),Q2)
        return [pi,pj,pk]

    def MakeColors(self,flavs,colI,colK):
        self.c += 1
        if flavs[2] == 21:
            return [ [self.c,colI[1]], [colK[1],self.c] ]

    def UpdateWeights(self,event):
        self.gs = []
        ants = [[e.id,e.cps[0]] for e in event if e.cps[0]!=0]
        gsum = mn(0)
        for i,iant in enumerate(ants):
            parI = event[iant[0]]
            parK = event[iant[1]]
            m2 = 2*parI.mom.SmallMLDP(parK.mom)
            if m2 <= 0 or m2 < 4*self.ct0: continue
            af = self.kernels[parI.pid][parK.pid]
            g = self.alphamax/(2*m.pi)*af.Integral(self.ct0/m2)
            if g <= 0: continue
            gsum += g
            self.gs.append([g,parI.id,parK.id,i,m2])
        self.gs.append([gsum,mn(0)])

    def GenerateZ(self,event,momsum,br,t):
        # Rapidity.
        m2  = br[3]
        xT  = t/m2
        if xT < 0 or xT > 1/4: return False
        xT0 = self.ct0/m2
        z   = br[2].GenerateZ(xT0)
        # Rejection step.
        if self.amode == 0:
            w = self.alpha(t,5)/self.alphamax
        else:
            asref = self.alpha.asa(t,5)
            if asref>0: w = self.alpha(t,5)/asref
            else: w = 0
        w *= br[2].Value(z,xT)/br[2].Estimate(z,xT)
        w *= 1/(1+self.beta)
        if w <= rng.random(): return False
        # Kinematics.
        phi = 2*m.pi*rng.random()
        moms = self.MakeKinematics(t,z,phi,br[0].mom,br[1].mom)
        if moms == []: return False
        # Branch.
        cps = []
        for cp in br[0].cps:
            if cp != 0: cps.append(cp)
        cols = self.MakeColors(br[2].flavs,br[0].col,br[1].col)
        event.append(Particle(br[2].flavs[2],moms[1],cols[1],len(event),[0,0]))
        for c in [0,1]:
            if cols[1][c] != 0 and cols[1][c] == br[0].col[c]:
                event[-1].cps[c] = br[0].cps[c]
                event[br[0].cps[c]].cps[1-c] = event[-1].id
                br[0].cps[c] = 0
        br[0].Set(br[2].flavs[0],moms[0],cols[0])
        for c in [0,1]:
            if cols[0][c] != 0 and cols[0][c] == cols[1][1-c]:
                event[-1].cps[1-c] = br[0].id
                br[0].cps[c] = event[-1].id
        br[1].mom = moms[2]
        br[2] = event[-1]
        self.UpdateWeights(event)
        return True

    def SelectSplit(self,event,rn1,rn2):
        ran = rn1*self.gs[-1][0]
        for g in self.gs[:-1]:
            ran -= g[0]
            if ran<=0:
                ant = [event[g[1]],event[g[2]],g[-1]]
                break
        af = self.kernels[ant[0].pid][ant[1].pid]
        return [ ant[0], ant[1], af, ant[2] ]

    def GeneratePoint(self,event):
        if len(self.gs) <= 1: return
        momsum = event[0].mom+event[1].mom
        trials = 0
        while self.t > self.ct0:
            trials += 1
            if trials == self.nmax:
                print('Abort after',trials,'trials in rank',comm.Get_rank())
                self.t = self.ct0
                return
            t = self.ct0
            g = self.gs[-1][0]
            if self.amode == 0:
                tt = self.t*mypow(mn(rng.random()),1/g)
            else:
                l2 = self.alpha.l2a(5)
                tt = l2*mypow(self.t/l2,mypow(mn(rng.random()),1/g))
            if tt > t:
                t = tt
                s = self.SelectSplit(event,rng.random(),rng.random())
            if len(event) == 4 and self.flat:
                lmax = min(self.lmax,self.alpha(self.Q2,5)/2*mylog(mn(1)/16))
                lmin = self.lmin
                lam = lmax+(lmin-lmax)*rng.random()
                t = self.Q2*myexp(lam/(self.alpha(self.Q2,5)/2))
                self.weight *= (lmax-lmin)*2/self.alpha(self.Q2,5)
                self.t = t
                while True:
                    if self.GenerateZ(event,momsum,s,t):
                        self.weight *= self.dsigma(t/self.Q2)
                        return
                    s = self.SelectSplit(event,rng.random(),rng.random())
            self.t = t
            if self.t > self.ct0:
                if self.GenerateZ(event,momsum,s,t): return

    def Run(self,event,nem):
        em = 0
        self.c = 2
        self.weight = 1
        self.ct0 = self.t0
        self.Q2 = (event[0].mom+event[1].mom).M2()
        self.t = self.Q2/4
        self.UpdateWeights(event)
        while self.t > self.ct0:
            if em >= nem: return
            self.GeneratePoint(event)
            if em == 0 and self.rt0 != 0:
                self.ct0 = max(self.t0,self.t*self.rt0)
            em += 1

import sys, time, optparse
from mpi4py import MPI

parser = optparse.OptionParser()
parser.add_option("-s","--seed",default=123456,dest="seed")
parser.add_option("-e","--events",default=1000,dest="events")
parser.add_option("-f","--file",default="vincia",dest="histo")
parser.add_option("-c","--collinear",default=3,dest="coll")
parser.add_option("-n","--nem",default=1000000,dest="nem")
parser.add_option("-N","--nmax",default=1000000,dest="nmax")
parser.add_option("-L","--lc",default=False,action="store_true",dest="lc")
parser.add_option("-a","--asmz",default='0.118',dest="asmz")
parser.add_option("-b","--beta",default='0',dest="beta")
parser.add_option("-A","--alphas",default=0.118,dest="alphas")
parser.add_option("-O","--order",default=1,dest="order")
parser.add_option("-M","--min",default=1,dest="min")
parser.add_option("-C","--cut",default=1,dest="cut")
parser.add_option("-R","--rcut",default='0',dest="rcut")
parser.add_option("-Q","--ecms",default='91.1876',dest="ecms")
parser.add_option("-F","--flat",default='[]',dest="flat")
parser.add_option("-q","--quad",default=0,action="count",dest="quad")
parser.add_option("-K","--cluster",default=5,dest="cas")
parser.add_option("-l","--logfile",default="",dest="logfile")
(opts,args) = parser.parse_args()

opts.histo = opts.histo.format(**vars(opts))
if opts.logfile != "":
    sys.stdout = open(opts.logfile, 'w')

comm = MPI.COMM_WORLD
if comm.Get_rank() == 0:
    print('Running on {} ranks'.format(comm.Get_size()))
import config
config.quad_precision = int(opts.quad)
from mymath import *
if comm.Get_rank() == 0: print_math_settings()

from vector import Vec4, Rotation, LT, GetRy, GetRz, Vec3, Mat3D
from particle import Particle, CheckEvent
from qcd import AlphaS, NC, TR, CA, CF
from analysis import Analysis

ecms = mn(opts.ecms)
lam = mn(opts.asmz)/mn(opts.alphas)
t0 = mypow(mn(opts.cut)/ecms**2,lam)*ecms**2
alphas = AlphaS(ecms,mn(opts.alphas),int(opts.order))
if comm.Get_rank() == 0:
    print("t_0 = {0}, log(Q^2/t_0) = {1}, \\alpha_s(t_0) = {2}". \
          format(t0,mylog(ecms**2/t0),alphas(t0)))
shower = Shower(alphas,t0,int(opts.coll),mn(opts.beta),
                mn(opts.rcut),eval(opts.flat),int(opts.nmax),opts.lc)
jetrat = Analysis(mn(opts.alphas),int(opts.cas))

rng.seed((comm.Get_rank()+1)*int(opts.seed))
start = time.perf_counter()
nevt, nout = int(float(opts.events)), 1
for i in range(1,nevt+1):
    event, weight = ( [
            Particle(-11,-Vec4(ecms/mn(2),mn(0),mn(0),ecms/mn(2)),[0,0],0),
            Particle(11,-Vec4(ecms/mn(2),mn(0),mn(0),-ecms/mn(2)),[0,0],1),
            Particle(1,Vec4(ecms/mn(2),mn(0),mn(0),ecms/mn(2)),[1,0],2,[3,0]),
            Particle(-1,Vec4(ecms/mn(2),mn(0),mn(0),-ecms/mn(2)),[0,1],3,[0,2])
        ], 1 )
    shower.Run(event,int(opts.nem))
    check = CheckEvent(event)
    if len(check): print('Error:',check[0],check[1])
    if i % nout == 0 and comm.Get_rank() == 0:
        if opts.logfile != "":
            print('Event {n}, {tp:.1f} s -> {tl:.1f} s left'.format
                  (n=i,tp=time.perf_counter()-start,
                   tl=(nevt-i)*(time.perf_counter()-start)/i
                   if i > 0 else 0))
        else:
            sys.stdout.write(
                '\rEvent {n}, {tp:.1f} s -> {tl:.1f} s left'.format
                (n=i,tp=time.perf_counter()-start,
                 tl=(nevt-i)*(time.perf_counter()-start)/i
                 if i > 0 else 0))
        sys.stdout.flush()
        if i/nout == 10: nout *= 10
    jetrat.Analyze(event,weight*shower.weight)
    if i % (nevt/10) == 0: jetrat.Finalize(opts.histo)
jetrat.Finalize(opts.histo)
if comm.Get_rank() == 0: print("")
