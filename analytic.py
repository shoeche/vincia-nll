import numpy as np
from scipy.special import gamma, hyp2f1

def Rp(aS,L,a,b,Ca):
    return 2 *Ca * 4 * (aS/(2*np.pi)) * L / (a*(a+b))

def Fadd(aS,L,a,b):
    rp = Rp(aS,L,a,b)
    return np.exp(-np.euler_gamma*rp)/gamma(1+rp)
    
def Fone(aS,L,a,b):
    return 1

def broadHypGeo(rp):
    return pow(2,rp/2) * rp * gamma(rp/2)/gamma(2+rp/2) * hyp2f1(1,2,2+rp/2,-1)

def FtotBroad(aS,L,a,b):
    rp = Rp(aS,L,a,b)
    return np.exp(-np.euler_gamma*rp)/gamma(1+rp) * broadHypGeo(rp)**2
    
def FwideBroad(aS,L,a,b):
    rp = Rp(aS,L,a,b)
    return np.exp(-np.euler_gamma*2*rp)/gamma(1+rp/2)**2 * broadHypGeo(rp)**2
    
def genResult(aS,L,a,b,F,Ca,Bl):
    return np.exp(-2*aS*Ca/(2*np.pi)*(L**2* 2/(a*(a+b)) + 4*Bl * L/(a+b)))*F(aS,L,a,b)

def resultThrust(aS,L):
    return genResult(aS,L,1,1,Fadd)

def resultFCx(aS,L,x):
    return genResult(aS,L,1,1-x,Fadd)

def resultY23(aS,L):
    return genResults(aS,L,2,0,Fone)

def resultTotBroad(aS,L):
    return genResults(aS,L,1,0,FtotBroad)

def resultWideBroad(aS,L):
    return genResults(aS,L,1,0,FwideBroad)
