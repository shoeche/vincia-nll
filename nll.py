from mymath import *

from qcd import AlphaS

class NLL:

    def __init__(self,alpha,Q2,a,b,Ca,coll):
        self.alpha = alpha
        self.Q2 = Q2
        self.a = a
        self.b = b
        self.Ca = Ca
        self.Bl = -3./4. if coll&1 else -1.

    def r1(self,as0,b0,a,b,L):
        l = as0*b0*L
        if b == 0:
            return -1/(2*m.pi*b0*l)*\
                (2*l/a+mylog(1-2*l/a))
        return 1/(2*m.pi*b0*l*b)*\
            ((a-2*l)*mylog(1-2*l/a)
             -(a+b-2*l)*mylog(1-2*l/(a+b)))

    def r(self,as0,b0,a,b,L):
        return L*self.r1(as0,b0,a,b,L)

    def T(self,as0,b0,L):
        l = as0*b0*L
        return -1/(m.pi*b0)*mylog(1-2*l)

    def R(self,v):
        L = mylog(1/v)
        Bl = -3./4.
        return 2*self.Ca*\
            (self.r(self.as0,self.b0,self.a,self.b,L)+\
             self.Bl*self.T(self.as0,self.b0,L/(self.a+self.b)))

    def rp(self,as0,b0,a,b,L):
        l = as0*b0*L
        if b == 0:
            return 2/a**2/(m.pi*b0)*l/(1-2*l/a)
        return 1/(m.pi*b0*b)*\
            (mylog(1-2*l/(a+b))-mylog(1-2*l/a))

    def Tp(self,as0,b0,L):
        l = as0*b0*L
        return 2*as0/m.pi/(1-2*l)

    def Rp(self,v):
        L = mylog(1/v)
        Bl = -3./4.
        return 2*self.Ca*\
            (self.rp(self.as0,self.b0,self.a,self.b,L)+\
             self.Bl/(self.a+self.b)*self.Tp(self.as0,self.b0,L/(self.a+self.b)))

    def sigma(self,v):
        if self.alpha.order < 0:
            aS = self.alpha(self.Q2,5)
            cx = mylog(v)
            J = self.a/(self.a+self.b)
            return myexp(-J*aS*self.Ca/(m.pi)*(cx**2/mn(2)-2*self.Bl*cx))
        self.as0 = self.alpha(self.Q2)
        self.b0 = self.alpha.beta0(5)/(2*m.pi)
        return m.exp(-self.R(v))

    def dsigma(self,v):
        if self.alpha.order < 0:
            aS = self.alpha(self.Q2,5)
            cx = mylog(v)
            J = self.a/(self.a+self.b)
            return myexp(-J*aS*self.Ca/(m.pi)*(cx**2/mn(2)-2*self.Bl*(cx+13/4-30*v)))* \
                         (-J*aS*self.Ca/(m.pi)*(cx-2*self.Bl*(1.-30*v)))
        self.as0 = self.alpha(self.Q2)
        self.b0 = self.alpha.beta0(5)/(2*m.pi)
        return self.Rp(v)*m.exp(-self.R(v))
